import React from "react";
import Title from "./components/Title";
import Form from "./components/Form";
import Weather from "./components/Weather";


//const API_KEY ="1a333d935e1e2fae98c0a3d580b0f265";

class App extends React.Component{
    state ={
        temperature: undefined,
        city:undefined,
        country:undefined,
        humidity:undefined,
        description:undefined,
        error:undefined 
    }
    //api call--------------->>>>>>//
    getWeather = async (e) => {
        e.preventDefault(); 
        const city = e.target.elements.city.value;
        const country = e.target.elements.country.value; 
        const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=1a333d935e1e2fae98c0a3d580b0f265&units=metric`);
        const data = await api_call.json();
        
        if(city && country){
            console.log(data);
        
            this.setState({
            temperature: data.main.temp,
            city : data.name,
            country : data.sys.country,
            humidity : data.main.humidity,
            description: data.weather[0].description,
            error:""
        });
      }else {
            this.setState({
            temperature: undefined,
            city : undefined,
            country : undefined,
            humidity : undefined,
            description: undefined,
            error:"Please Enter The Value."
            });
      }
    }
    render(){
        return(
          
            <div> 
              <div className="wrapper">
                <div className="main">
                  <div className="container">
                     <div className="row">
                       <div className="col-xs-5 title-container">
                            <Title />
                        </div>
                        <div className="col-xs-7 form-container">
                            <Form getWeather={this.getWeather}/>
                                 < Weather
                                    temperature={this.state.temperature}
                                    city={this.state.city}
                                    country={this.state.country}
                                    humidity={this.state.humidity}
                                    description={this.state.description}
                                    error={this.state.error}
                                />
                        </div>
                      </div>   
                  </div>
                 </div>
              </div>
            </div>
        );
    }
};

             

//api.openweathermap.org/data/2.5/weather?q=Manchester,uk&appid=1a333d935e1e2fae98c0a3d580b0f265&units=metric
export default App;